import type { FilterParams } from "@/products/models/interfaces/filter-params.interface";
import type { Product } from "@/products/models/types/product.type";

export const getProducts = async (
  params: FilterParams
): Promise<{ products: Product[]; maxPages: number }> => {
  const queryParams = new URLSearchParams();
  queryParams.set("offset", params.offset ?? "0");
  queryParams.set("limit", params.limit ?? "10");
  if (params.title) queryParams.set("title", params.title);
  if (params.categoryId) queryParams.set("categoryId", params.categoryId);
  const response = await fetch(`${import.meta.env.PUBLIC_API_URL}v1/products?${queryParams}`);
  const isOk = response.ok;
  const json = await response.json();
  if (!isOk) throw new Error("Failed request");
  return json;
};

export const getProduct = async (id: number): Promise<{ product: Product }>  => {
  const response = await fetch(`${import.meta.env.PUBLIC_API_URL}v1/products/${id}`);
  const isOk = response.ok;
  const json = await response.json();
  if (!isOk) throw new Error("Failed request");
  return json;
} 
