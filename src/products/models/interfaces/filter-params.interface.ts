export interface FilterParams {
  limit?: string | null;
  offset?: string | null;
  title?: string | null;
  categoryId?: string | null;
}
