import { products as productsJson } from "@/mock/products.json";

const getProducts = (categoryId: number | null, title: string | null) => {
  let products = productsJson;
  if (categoryId && title)
    products = products.filter(
      (product) =>
        product.category.id === Number(categoryId) &&
        product.title.toLowerCase().includes(title.toLowerCase())
    );
  else if (categoryId) {
    products = products.filter((product) => product.category.id === Number(categoryId));
  } else if (title) {
    products = products.filter((product) =>
      product.title.toLowerCase().includes(title.toLowerCase())
    );
  }
  return products;
};

const paginationProducts = (products: any, limit: number, offset: number) => {
  const maxPages = Math.ceil(products.length / limit);
  return { products: products.slice(offset, offset + limit), maxPages: maxPages };
};

export async function GET({ url: { searchParams } }: { url: { searchParams: URLSearchParams } }) {
  const categoryId = searchParams.get("categoryId") ? Number(searchParams.get("categoryId")) : null;
  const allProducts = getProducts(categoryId, searchParams.get("title"));
  return new Response(
    JSON.stringify(
      paginationProducts(
        allProducts,
        Number(searchParams.get("limit") ?? "10"),
        Number(searchParams.get("offset") ?? "0")
      )
    )
  );
}
