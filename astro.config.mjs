import netlify from '@astrojs/netlify';
import tailwind from "@astrojs/tailwind";
import { defineConfig } from "astro/config";

// https://astro.build/config
export default defineConfig({
  verbatimModuleSyntax: false,
  integrations: [tailwind()],
  output: "server",
  imports: {
    "@/": "src"
  },
  adapter: netlify()
});