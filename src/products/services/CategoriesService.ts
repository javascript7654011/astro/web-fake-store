import type { Category } from "@/products/models/types/category.type";

export const getCategories = async (): Promise<{ categories: Category[] }> => {
  const response = await fetch(`${import.meta.env.PUBLIC_API_URL}v1/categories`);
  const isOk = response.ok;
  const json = await response.json();
  if (!isOk) throw new Error("Failed request");
  return json;
};
