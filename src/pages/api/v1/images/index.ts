export async function GET({ url: { searchParams }}) {
  const url = searchParams.get('url');
  const response = await fetch(url);
  return new Response(await response.arrayBuffer());
}
