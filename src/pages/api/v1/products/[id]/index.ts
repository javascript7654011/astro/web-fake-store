import { products as productsJson } from "@/mock/products.json";

export async function GET({ params: { id } }) {
  const product = productsJson.find(product => product.id === Number(id));
  if(!product) return new Response(JSON.stringify({ detail: "Not found product"}), { status: 404 });
  return new Response(JSON.stringify({ product }), { status: 201 });
}
