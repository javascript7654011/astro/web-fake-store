export function formatPrice(price: number) {
  const [integerPart, decimalPart] = price.toFixed(2).toString().split(".");
  const formattedIntegerPart = integerPart.replace(/\B(?=(\d{3})+(?!\d))/g, ".");
  return `$ ${formattedIntegerPart},${decimalPart}`;
}