export type Category = {
  id: number;
  name: "Clothes";
  image: "https://i.imgur.com/QkIa5tT.jpeg";
  creationAt: "2024-03-15T17:59:44.000Z";
  updatedAt: "2024-03-15T17:59:44.000Z";
};
