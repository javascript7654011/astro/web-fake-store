import { categories } from "@/mock/categories.json";

export async function GET() {
  return new Response(
    JSON.stringify({
      categories,
    })
  );
}
